﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />

    <asp:Label ID="labelIDofNum" runat="server" />
    <br />
    <asp:Label ID="labelNameofString" runat="server" />
    <br />
    <asp:Label ID="labelContentofString" runat="server" />

    <br />

    <asp:PlaceHolder ID="PlaceHolder1" runat="server" />
    <br />

    <asp:Button id="clickBtnAdd" text="To Add page" runat="server" OnClick="Click_Me"/>

    <br />
    <br />

    <asp:GridView ID="gvPageList" runat="server" AutoGenerateColumns="false" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None"
        DataKeyNames="pageid" ShowFooter="false" 
        OnRowEditing="gvPageList_RowEditing" OnRowCancelingEdit="gvPageList_RowCancelingEdit"
        OnRowUpdating="gvPageList_RowUpdating"
        OnRowDeleting="gvPageList_RowDeleting">

        <AlternatingRowStyle BackColor="PaleGoldenrod" />
        <FooterStyle BackColor="Tan" />
        <HeaderStyle BackColor="Tan" Font-Bold="True" />
        <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
        <SortedAscendingCellStyle BackColor="#FAFAE7" />
        <SortedAscendingHeaderStyle BackColor="#DAC09E" />
        <SortedDescendingCellStyle BackColor="#E1DB9C" />
        <SortedDescendingHeaderStyle BackColor="#C2A47B" />

        <Columns>

            <asp:TemplateField HeaderText="Title">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("pagetitle") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtPageTitle" Text='<%# Eval("pagetitle") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtPageNameFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Description">
                <ItemTemplate>
                    <asp:Label Text='<%# Eval("pagecontent") %>' runat="server"></asp:Label>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:TextBox ID="txtPageContent" Text='<%# Eval("pagecontent") %>' runat="server" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtPageContentFooter" runat="server" />
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Updation">
                <ItemTemplate>
                    <asp:LinkButton text="Edit" runat="server"  ToolTip="Click Edit button"  Height="30px" Width="45px" CommandName="Edit" />
                    <asp:LinkButton text="Delete" runat="server" ToolTip="Delete Button" Height="30px"  Width="45px"  CommandName="Delete"/>
                </ItemTemplate>
                <EditItemTemplate>
                    <asp:LinkButton text="Save" runat="server"  ToolTip="Click Update Button" Height="30px"  Width="45px" CommandName="Update" />
                    <asp:LinkButton text="Cancel" runat="server" ToolTip="Click Cancel Button"  Height="30px" Width="45px"  CommandName="Cancel" />
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton text="Add New" runat="server" ToolTip="Click Add New Button"  Height="30px" Width="100px" CommandName="AddNew" />
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>


</asp:Content>
