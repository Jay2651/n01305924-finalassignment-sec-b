﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class _Default : Page
    {

        string constr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\JAY PATEL\Desktop\All Project\All Project\WebApplication2\WebApplication2\WebApplication2\App_Data\Database1.mdf;Integrated Security=True";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["pageid"] == null)
            {
                gvPageList.Visible = true;
                PlaceHolder1.Visible = true;
                labelIDofNum.Visible = false;
                labelNameofString.Visible = false;
                labelContentofString.Visible = false;
                clickBtnAdd.Visible = true; 
            }
            else
            {
                gvPageList.Visible = false;
                PlaceHolder1.Visible = false;
                clickBtnAdd.Visible = false;

                labelIDofNum.Visible = true;
                labelNameofString.Visible = true;
                labelContentofString.Visible = true;

                labelIDofNum.Text = "Display Of ID : " + Request.QueryString["pageid"];
                labelNameofString.Text = "Display Of Name : " + Request.QueryString["enterName"];
                labelContentofString.Text = "Display Of Data : " + Request.QueryString["enterData"];
            }

            if (!this.IsPostBack)
            {
                fillGridView();
            }
        }

        public void fillGridView()
        {
            DataTable dtTable = this.GetData();

            StringBuilder htmlString = new StringBuilder();

            htmlString.Append("<table>");
            htmlString.Append("<tr>");

            foreach (DataRow row in dtTable.Rows)
            {
                htmlString.Append("<td><a href='Default.aspx?pageid=" + row[0] + "&enterName=" + row[1] + "&enterData=" + row[2] + "'>");
                htmlString.Append(row[1]);
                htmlString.Append("</a> &nbsp &nbsp</td>");
            }
            htmlString.Append("</tr>");
            htmlString.Append("</table>");

            PlaceHolder1.Controls.Add(new Literal { Text = htmlString.ToString() });

            if (dtTable.Rows.Count > 0)
            {
                gvPageList.DataSource = dtTable;
                gvPageList.DataBind();
            }
            else
            {
                dtTable.Rows.Add(dtTable.NewRow());
                gvPageList.DataSource = dtTable;
                gvPageList.DataBind();

                gvPageList.Rows[0].Cells.Clear();
                gvPageList.Rows[0].Cells.Add(new TableCell());
                gvPageList.Rows[0].Cells[0].ColumnSpan = dtTable.Columns.Count;
                gvPageList.Rows[0].Cells[0].Text = "No Data Found ..!";
                gvPageList.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;
            }
        }

        private DataTable GetData()
        {
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = new SqlCommand("SELECT pageid, pagetitle, pagecontent FROM page", con);
            SqlDataAdapter sd = new SqlDataAdapter();

            sd.SelectCommand = cmd;

            DataTable dtTable = new DataTable();
            sd.Fill(dtTable);
            con.Close();

            return dtTable;
        }

        protected void gvPageList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = new SqlCommand("DELETE FROM page WHERE pageid = @id", con);
            cmd.Parameters.AddWithValue("@id", Convert.ToInt32(gvPageList.DataKeys[e.RowIndex].Value.ToString()));
            cmd.ExecuteNonQuery();

            fillGridView();
            con.Close();
        }

        //protected void gvPageList_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName.Equals("AddNew"))
        //    {
        //        SqlConnection con = new SqlConnection(constr);
        //        con.Open();

        //        SqlCommand cmd = new SqlCommand("INSERT INTO page (pagetitle,pagecontent) VALUES (@pagetitle,@pagecontent)", con);
        //        cmd.Parameters.AddWithValue("@pagetitle", (gvPageList.FooterRow.FindControl("txtPageNameFooter") as TextBox).Text.Trim());
        //        cmd.Parameters.AddWithValue("@pagecontent", (gvPageList.FooterRow.FindControl("txtPageContentFooter") as TextBox).Text.Trim());
        //        cmd.ExecuteNonQuery();

        //        fillGridView();
        //        con.Close();
        //    }
        //}

        protected void gvPageList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvPageList.EditIndex = e.NewEditIndex;
            fillGridView();
        }

        protected void gvPageList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvPageList.EditIndex = -1;
            fillGridView();
        }

        protected void gvPageList_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = new SqlCommand("UPDATE page SET pagetitle=@pagetitle,pagecontent=@pagecontent WHERE pageid = @id", con);

            cmd.Parameters.AddWithValue("@pagetitle", (gvPageList.Rows[e.RowIndex].FindControl("txtPageTitle") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("@pagecontent", (gvPageList.Rows[e.RowIndex].FindControl("txtPageContent") as TextBox).Text.Trim());
            cmd.Parameters.AddWithValue("@id", Convert.ToInt32(gvPageList.DataKeys[e.RowIndex].Value.ToString()));

            cmd.ExecuteNonQuery();

            gvPageList.EditIndex = -1;
            fillGridView();

            con.Close();

        }

        protected void Click_Me(object sender, EventArgs e)
        {
            Response.Redirect("DataAdd.aspx");
        }
    }
}