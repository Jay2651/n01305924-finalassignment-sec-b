﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class AddNew : System.Web.UI.Page
    {
        string constr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\JAY PATEL\Desktop\All Project\All Project\WebApplication2\WebApplication2\WebApplication2\App_Data\Database1.mdf;Integrated Security=True";

        protected void Page_Load(object sender, EventArgs e)
        {
            Content.Text = "";
        }

        protected void click_btn_add(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            if (name.Text != "" && title.Text != "")
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO page (pagetitle,pagecontent) VALUES (@pagetitle,@pagecontent)", con);
                cmd.Parameters.AddWithValue("@pagetitle", (name.Text.ToString()));
                cmd.Parameters.AddWithValue("@pagecontent", (title.Text.ToString()));
                cmd.ExecuteNonQuery();
                con.Close();
                Response.Redirect("Default.aspx");
            }
            else
            {
                Content.Text = "Please enter all detail";
            }
        }
    }
}