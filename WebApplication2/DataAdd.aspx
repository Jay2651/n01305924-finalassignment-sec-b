﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DataAdd.aspx.cs" Inherits="WebApplication2.AddNew" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="Content" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Name" ID="lblName" runat="server" /></td>
            <td>
                <asp:TextBox ID="name" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label Text="Title" ID="lblContent" runat="server" /></td>
            <td>
                <asp:TextBox ID="title" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnAdd" Text="Submit to ADD" runat="server" OnClick="click_btn_add" /></td>
        </tr>

    </table>
</asp:Content>
